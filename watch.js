"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = watch;

var _build = require("./build");

var _build2 = _interopRequireDefault(_build);

var _lodash = require("lodash");

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
* The watch function allows easy of access to boiler plate gulp code.
* This allows a developer to quickly put together an ES6 code base and transform it to ES5 for compatibility.
* This uses the Babel engine to transpile the code.
*
*
* @param {Object} options - configuration for build function.
* @param {String} [options.src="./src"] - The location of the src document.
* @param {String} [options.outDir="."] - The location of the compiled files.
* @param {String} [options.watch=true] - watch is a parameter to the build function as well but it is undocumented.
* @return {null}
*/

function watch(options, cb) {
  if (_lodash2.default.isFunction(options)) {
    cb = options;
    options = null;
  }

  if (_lodash2.default.isNil(options)) {
    options = {};
  }

  options = _lodash2.default.defaults({ watch: true }, options);

  (0, _build2.default)(options);
  cb();
}