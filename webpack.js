"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require("babel-runtime/helpers/slicedToArray");

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

exports.build = build;
exports.watch = watch;

var _lodash = require("lodash");

var _lodash2 = _interopRequireDefault(_lodash);

var _child_process = require("child_process");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var webpack = require.resolve("webpack/bin/webpack");

function fixArgs(options, cb) {
    if (_lodash2.default.isNil(cb)) {
        cb = function cb() {};
    }

    if (_lodash2.default.isFunction(options)) {
        cb = options;
        options = null;
    }

    if (_lodash2.default.isNil(options)) {
        options = {};
    }

    options = _lodash2.default.defaults(options, {
        stdio: "inherit"
    });

    return [options, cb];
}

/**
 *
 * This function is used to automated webpack building. This expects the a webpack.config.js file to be used for the settings.
 *
 *
 * @param {Object} options - These options are used for the spawn function from node
 * @param {Boolean} options.watch - Specifies whether the directory should be watched
 * @param {Function} cb - The callback function to be used once completed.
 * @return {ChildProcess}
 */
function build(options, cb) {
    var _fixArgs = fixArgs(options, cb);

    var _fixArgs2 = (0, _slicedToArray3.default)(_fixArgs, 2);

    options = _fixArgs2[0];
    cb = _fixArgs2[1];

    var commands = [webpack];

    if (options.watch) {
        commands.push("--watch");
    }

    var child = (0, _child_process.spawn)(_lodash2.default.head(process.argv), commands, options);

    child.on("exit", function (code) {
        cb(null, code);
    });

    return child;
}

/**
 *
 * This function is used to automated webpack building. This expects the a webpack.config.js file to be used for the settings.
 *
 *
 * @param {Object} options - These options are used for the spawn function from node
 * @param {Boolean} [options.watch=true] - Specifies whether the directory should be watched
 * @param {Function} cb - The callback function to be used once completed.
 * @return {ChildProcess}
 */

function watch(options, cb) {
    var child;

    var _fixArgs3 = fixArgs(options, cb);

    var _fixArgs4 = (0, _slicedToArray3.default)(_fixArgs3, 2);

    options = _fixArgs4[0];
    cb = _fixArgs4[1];

    options.watch = true;
    child = build(options);
    child.on("exit", function () {
        watch(options, cb);
    });
}