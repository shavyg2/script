"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Register;
exports.build = build;
exports.build_debug = build_debug;

var _gulpNewer = require("gulp-newer");

var _gulpNewer2 = _interopRequireDefault(_gulpNewer);

var _gulpBabel = require("gulp-babel");

var _gulpBabel2 = _interopRequireDefault(_gulpBabel);

var _gulpHotPepper = require("gulp-hot-pepper");

var _gulpHotPepper2 = _interopRequireDefault(_gulpHotPepper);

var _gulpPlumber = require("gulp-plumber");

var _gulpPlumber2 = _interopRequireDefault(_gulpPlumber);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * Registers quickly will register the build and the build.debug function
 *
 * @param {GulpObject} gulp - A reference to the current gulp being used in your project
 * @param {Object} options - options to pass to the register function. Options are passed to the build and build_debug function
 * @return {null}
 */
function Register(gulp, options) {
    build(gulp, options);
    build_debug(gulp, options);
}

/**
 * build will register the build gulp command. This allows gulp to build an ES6 collection of files to ES5.
 * This will use the babel plugin. This also adds a gulp watch command to the same
 * directories so that automatic building can take place
 *
 *
 * @param {GulpObject} gulp - A reference to the current gulp being used in your project
 * @param {Object} options - Options to pass to the build function
 * @param {String} [options.src="/**\/*.js"] - a glob pattern of the files to transpile
 * @param {String} [options.outDir="."] - a glob pattern of the files to transpile
 * @param {String[]} [options.presets=["es2015","stage-0"]] - Babel presets to be used.
 * @param {String[]} [options.plugins=["transform-runtime"] - Babel plugins to be used.
 * @return {null}
 */

function build(gulp, options) {
    options = options || {};
    var outdir = options.outDir || ".";
    gulp.task("build", function () {
        return gulp.src(options.src || "src/**/*.js").pipe((0, _gulpBabel2.default)({
            presets: options.presets || ["es2015", "stage-0"],
            plugins: options.plugins || ["transform-runtime"]
        })).pipe(gulp.dest(outdir));
    });

    gulp.task("watch", ["build"], function () {
        gulp.watch("src/**/*.js", ["build"]);
    });
}

/**
 * build_debug will register the build.debug gulp command. This allows gulp to build an ES6 collection of files to ES5.
 * This will use the babel plugin. This also adds a gulp watch.debug command to the same
 * directories so that automatic building can take place
 *
 *
 * @param {GulpObject} gulp - A reference to the current gulp being used in your project
 * @param {Object} options - Options to pass to the build function
 * @param {String} [options.src="/**\/*.js"] - a glob pattern of the files to transpile
 * @param {String} [options.outDir="."] - a glob pattern of the files to transpile
 * @param {String[]} [options.presets=["es2015","stage-0"]] - Babel presets to be used.
 * @param {String[]} [options.plugins=["transform-runtime"]] - Babel plugins to be used.
 * @param {String} [options.logfile="logger.log"] - Babel plugins to be used.
 * @return {null}
 */
function build_debug(gulp, options) {
    options = options || {};
    var outdir = options.outDir || ".";
    gulp.task("build.debug", function () {
        return gulp.src(options.src || "src/**/*.js").pipe((0, _gulpPlumber2.default)()).pipe((0, _gulpHotPepper2.default)({
            "hot-pepper-logger": "hot-pepper-logger-interface/lib/FileLogInterface",
            "hot-pepper-logger-options": {
                file: options.logfile || "logger.log"
            },
            "plugins": {
                asyncawait: true
            },
            "extend": ["acorn-es7-plugin"]
        })).pipe((0, _gulpBabel2.default)({
            presets: ["es2015", "stage-0"],
            plugins: ["transform-runtime"]
        })).pipe(gulp.dest(outdir));
    });

    gulp.task("watch.debug", ["build.debug"], function () {
        gulp.watch("src/**/*.js", ["build.debug"]);
    });
}