"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = build;

var _child_process = require("child_process");

var _lodash = require("lodash");

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var node = process.argv[0];
var babel = require.resolve("babel-cli/bin/babel");

/**
* The build function allows easy of access to boiler plate gulp code.
* This allows a developer to quickly put together an ES6 code base and transform it to ES5 for compatibility.
* This uses the Babel engine to transpile the code.
*
*
* @param {Object} options - configuration for build function.
* @param {String} [options.src="./src"] - The location of the src document.
* @param {String} [options.outDir="."] - The location of the compiled files.
* @return {null}
*/
function build() {
    var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var cb = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];

    if (_lodash2.default.isFunction(options)) {
        cb = options;
        options = null;
    }

    if (_lodash2.default.isNil(options)) {
        options = {};
    }

    var command = ["" + babel, options.src || "./src", "--out-dir", "" + (options.out_dir || options.outDir || '.')];

    if (options.watch) {
        command.push("--watch");
    }

    var child = (0, _child_process.spawn)(node, command, {
        stdio: "inherit"
    });

    child.on("error", function (e) {
        cb(e);
    });
    if (!options.watch) {
        child.on("exit", function (code) {
            if (_lodash2.default.isFunction(cb)) {
                cb(null, code);
            }
        });
    }
}