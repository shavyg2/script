"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof2 = require("babel-runtime/helpers/typeof");

var _typeof3 = _interopRequireDefault(_typeof2);

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _promise = require("babel-runtime/core-js/promise");

var _promise2 = _interopRequireDefault(_promise);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

exports.default = lint;

var _lodash = require("lodash");

var _lodash2 = _interopRequireDefault(_lodash);

var _jshint = require("jshint");

var _glob = require("glob");

var _glob2 = _interopRequireDefault(_glob);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function lint(options, cb) {
    var _this = this;

    if (_lodash2.default.isFunction(options)) {
        cb = options;
        options = null;
    }

    if (_lodash2.default.isNil(options)) {
        options = {};
    }

    var conf = _fs2.default.readFileSync(".jshintrc", "utf8");
    conf = JSON.parse(conf);

    options.src = options.src || "./src/**/*.js";

    (0, _glob2.default)(options.src, function () {
        var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(err, files) {
            var p, done;
            return _regenerator2.default.wrap(function _callee3$(_context3) {
                while (1) {
                    switch (_context3.prev = _context3.next) {
                        case 0:
                            p = (0, _lodash2.default)(files).map(function () {
                                var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(file) {
                                    var fileContents;
                                    return _regenerator2.default.wrap(function _callee$(_context) {
                                        while (1) {
                                            switch (_context.prev = _context.next) {
                                                case 0:
                                                    _context.prev = 0;
                                                    _context.next = 3;
                                                    return new _promise2.default(function (resolve, reject) {
                                                        _fs2.default.readFile(file, "utf8", function (err, content) {
                                                            if (err) {
                                                                return reject(err);
                                                            }
                                                            resolve(content);
                                                        });
                                                    });

                                                case 3:
                                                    fileContents = _context.sent;
                                                    return _context.abrupt("return", { file: file, fileContents: fileContents });

                                                case 7:
                                                    _context.prev = 7;
                                                    _context.t0 = _context["catch"](0);

                                                case 9:
                                                case "end":
                                                    return _context.stop();
                                            }
                                        }
                                    }, _callee, _this, [[0, 7]]);
                                }));
                                return function (_x3) {
                                    return ref.apply(this, arguments);
                                };
                            }()).
                            // console.log(e.message);
                            map(function () {
                                var ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(p) {
                                    var content, result;
                                    return _regenerator2.default.wrap(function _callee2$(_context2) {
                                        while (1) {
                                            switch (_context2.prev = _context2.next) {
                                                case 0:
                                                    _context2.prev = 0;
                                                    _context2.next = 3;
                                                    return p;

                                                case 3:
                                                    content = _context2.sent;

                                                    if (_lodash2.default.isString(content.fileContents)) {
                                                        _context2.next = 6;
                                                        break;
                                                    }

                                                    throw new Error("\"content must be string " + (typeof content === "undefined" ? "undefined" : (0, _typeof3.default)(content)) + "\"");

                                                case 6:
                                                    result = (0, _jshint.JSHINT)(content.fileContents, conf);
                                                    return _context2.abrupt("return", _jshint.JSHINT.errors);

                                                case 10:
                                                    _context2.prev = 10;
                                                    _context2.t0 = _context2["catch"](0);

                                                    console.log(_context2.t0.message);

                                                case 13:
                                                case "end":
                                                    return _context2.stop();
                                            }
                                        }
                                    }, _callee2, _this, [[0, 10]]);
                                }));
                                return function (_x4) {
                                    return ref.apply(this, arguments);
                                };
                            }()).value();
                            _context3.next = 3;
                            return _promise2.default.all(p);

                        case 3:
                            done = _context3.sent;

                            console.log(done);

                        case 5:
                        case "end":
                            return _context3.stop();
                    }
                }
            }, _callee3, _this);
        }));
        return function (_x, _x2) {
            return ref.apply(this, arguments);
        };
    }());
}