import build from "./build";
import _ from "lodash";



/**
* The watch function allows easy of access to boiler plate gulp code.
* This allows a developer to quickly put together an ES6 code base and transform it to ES5 for compatibility.
* This uses the Babel engine to transpile the code.
*
*
* @param {Object} options - configuration for build function.
* @param {String} [options.src="./src"] - The location of the src document.
* @param {String} [options.outDir="."] - The location of the compiled files.
* @param {String} [options.watch=true] - watch is a parameter to the build function as well but it is undocumented.
* @return {null}
*/

export default function watch(options,cb){
  if(_.isFunction(options)){
    cb = options;
    options=null;
  }


  if(_.isNil(options)){
    options = {};
  }

  options = _.defaults({watch:true},options);

  build(options);
  cb();
}
