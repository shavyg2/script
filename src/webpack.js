import _ from "lodash";
import {
    spawn
} from "child_process";
const webpack = require.resolve("webpack/bin/webpack");


function fixArgs(options, cb) {
    if (_.isNil(cb)) {
        cb = function() {};
    }

    if (_.isFunction(options)) {
        cb = options;
        options = null;
    }

    if (_.isNil(options)) {
        options = {};
    }


    options = _.defaults(options, {
        stdio: "inherit"
    })


    return [options, cb];
}


/**
 *
 * This function is used to automated webpack building. This expects the a webpack.config.js file to be used for the settings.
 *
 *
 * @param {Object} options - These options are used for the spawn function from node
 * @param {Boolean} options.watch - Specifies whether the directory should be watched
 * @param {Function} cb - The callback function to be used once completed.
 * @return {ChildProcess}
 */
export function build(options, cb) {

    [options, cb] = fixArgs(options, cb);
    var commands = [webpack];

    if (options.watch) {
        commands.push("--watch");
    }


    var child = spawn(_.head(process.argv), commands, options);

    child.on("exit", function(code) {
        cb(null, code);
    })

    return child;
}

/**
 *
 * This function is used to automated webpack building. This expects the a webpack.config.js file to be used for the settings.
 *
 *
 * @param {Object} options - These options are used for the spawn function from node
 * @param {Boolean} [options.watch=true] - Specifies whether the directory should be watched
 * @param {Function} cb - The callback function to be used once completed.
 * @return {ChildProcess}
 */

export function watch(options, cb) {
    var child;
    [options, cb] = fixArgs(options, cb);
    options.watch = true;
    child = build(options);
    child.on("exit", function() {
        watch(options, cb);
    })

}
