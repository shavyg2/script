import {
    spawn
} from "child_process";
import _ from "lodash";
const node = process.argv[0];
const babel = require.resolve("babel-cli/bin/babel");



/**
* The build function allows easy of access to boiler plate gulp code.
* This allows a developer to quickly put together an ES6 code base and transform it to ES5 for compatibility.
* This uses the Babel engine to transpile the code.
*
*
* @param {Object} options - configuration for build function.
* @param {String} [options.src="./src"] - The location of the src document.
* @param {String} [options.outDir="."] - The location of the compiled files.
* @return {null}
*/
export default function build(options = {}, cb = null) {
    if (_.isFunction(options)) {
        cb = options;
        options = null;
    }


    if (_.isNil(options)) {
        options = {};
    }



    let command = [
        `${babel}`,
        options.src||"./src",
        `--out-dir`,
        `${options.out_dir||options.outDir||'.'}`
    ]


    if (options.watch) {
        command.push("--watch");
    }




    var child = spawn(node, command, {
        stdio: "inherit"
    });



    child.on("error",function(e){
      cb(e);
    })
    if (!options.watch) {
        child.on("exit", function(code) {
            if (_.isFunction(cb)) {
                cb(null, code);
            }
        })
    }
}
