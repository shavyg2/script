import newer from "gulp-newer";
import babel from "gulp-babel";
import gulphp from "gulp-hot-pepper";
import plumber from "gulp-plumber";


/**
 *
 * Registers quickly will register the build and the build.debug function
 *
 * @param {GulpObject} gulp - A reference to the current gulp being used in your project
 * @param {Object} options - options to pass to the register function. Options are passed to the build and build_debug function
 * @return {null}
 */
export default function Register(gulp, options) {
    build(gulp, options);
    build_debug(gulp, options);
}


/**
 * build will register the build gulp command. This allows gulp to build an ES6 collection of files to ES5.
 * This will use the babel plugin. This also adds a gulp watch command to the same
 * directories so that automatic building can take place
 *
 *
 * @param {GulpObject} gulp - A reference to the current gulp being used in your project
 * @param {Object} options - Options to pass to the build function
 * @param {String} [options.src="/**\/*.js"] - a glob pattern of the files to transpile
 * @param {String} [options.outDir="."] - a glob pattern of the files to transpile
 * @param {String[]} [options.presets=["es2015","stage-0"]] - Babel presets to be used.
 * @param {String[]} [options.plugins=["transform-runtime"] - Babel plugins to be used.
 * @return {null}
 */

export function build(gulp, options) {
    options = options || {};
    let outdir = options.outDir || ".";
    gulp.task("build", function() {
        return gulp.src(options.src || "src/**/*.js").
        pipe(babel({
            presets: options.presets || ["es2015", "stage-0"],
            plugins: options.plugins || ["transform-runtime"]
        })).
        pipe(gulp.dest(outdir));
    })


    gulp.task("watch", ["build"], function() {
        gulp.watch("src/**/*.js", ["build"]);
    })
}



/**
 * build_debug will register the build.debug gulp command. This allows gulp to build an ES6 collection of files to ES5.
 * This will use the babel plugin. This also adds a gulp watch.debug command to the same
 * directories so that automatic building can take place
 *
 *
 * @param {GulpObject} gulp - A reference to the current gulp being used in your project
 * @param {Object} options - Options to pass to the build function
 * @param {String} [options.src="/**\/*.js"] - a glob pattern of the files to transpile
 * @param {String} [options.outDir="."] - a glob pattern of the files to transpile
 * @param {String[]} [options.presets=["es2015","stage-0"]] - Babel presets to be used.
 * @param {String[]} [options.plugins=["transform-runtime"]] - Babel plugins to be used.
 * @param {String} [options.logfile="logger.log"] - Babel plugins to be used.
 * @return {null}
 */
export function build_debug(gulp, options) {
    options = options || {};
    let outdir = options.outDir || ".";
    gulp.task("build.debug", function() {
        return gulp.src(options.src || "src/**/*.js").
        pipe(plumber()).
        pipe(gulphp({
            "hot-pepper-logger": "hot-pepper-logger-interface/lib/FileLogInterface",
            "hot-pepper-logger-options": {
                file: options.logfile || "logger.log"
            },
            "plugins": {
                asyncawait: true
            },
            "extend": ["acorn-es7-plugin"]
        })).
        pipe(babel({
            presets: ["es2015", "stage-0"],
            plugins: ["transform-runtime"]
        })).
        pipe(gulp.dest(outdir));
    })

    gulp.task("watch.debug", ["build.debug"], function() {
        gulp.watch("src/**/*.js", ["build.debug"])
    })
}
