import _ from "lodash";
import {JSHINT} from "jshint";
import glob from "glob";
import fs from "fs";


export default function lint(options, cb) {
    if (_.isFunction(options)) {
        cb = options;
        options = null;
    }


    if (_.isNil(options)) {
        options = {};
    }

    let conf = fs.readFileSync(".jshintrc","utf8");
    conf = JSON.parse(conf);

    options.src = options.src || "./src/**/*.js";


    glob(options.src, async (err, files) => {
        let p = _(files).map(async file => {
            try {

              let fileContents = await new Promise(function(resolve, reject) {
                fs.readFile(file, "utf8", (err, content) => {
                  if (err) {
                    return reject(err);
                  }
                  resolve(content);
                });
              });
              return {file,fileContents};
            } catch (e) {
                // console.log(e.message);
            }
        }).map(async (p)=>{
          try {
            let content = await p;
            if(!_.isString(content.fileContents)){
              throw new Error(`"content must be string ${typeof content}"`);
            }
            var result = JSHINT(content.fileContents,conf);
            return JSHINT.errors;
          } catch (e) {
            console.log(e.message);
          }
        }).value();
        var done = await Promise.all(p);
        console.log(done);
    })
}
