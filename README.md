# Script

This library can be used with node to automate some build processes for ES6\. This reduces the amount of boilerplate code that you have to use.

```
npm run build
npm run doc
npm run readdoc

```
